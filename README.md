# Parking project

Тренировочный проект по бронированию парковочных мест

## Макет интерфейса (Figma)
- Файл с названием test tasck.fig (допустимы откланения от макета всилоу того что отдает бэк)
- Большинство компонент статично;
- Активна работа с таблицей (Удаление записей при выделении, редактирование записи по двойному клику по строке с записью);
- Активна работа с формой добавления записей.

### Основные технологии

- TypeScript
  - Основная документация [Typescriptlang.org](https://www.typescriptlang.org/docs/handbook/basic-types.html)
  - Enum в частности [Enum](https://www.typescriptlang.org/docs/handbook/enums.html)
- React / Redux / Стор
  - [Reactjs.org](https://reactjs.org/)
  - [Redux.js.org](https://redux.js.org/)
  - Оптимизация работы со стором - React-select [Reselect](https://github.com/reduxjs/reselect)
- Асинхронное взаимодействие с сервером - [Sagas](https://redux-saga.js.org/docs/introduction/BeginnerTutorial.html)
- Компоненты [MaterialUi](https://material-ui.com/ru/getting-started/usage/)
- Время и даты [MomentJS](https://momentjs.com/docs/)

## Бэк проекта
- Лежит в папке backOfCars
- Требует для запуска java 11
- Запускается командой java -jar parking.jar из cmd