import {CarsActions} from '../constants/ActionTypes';
import * as actions from '../actions/CarsActions';

type InferValueTypes<T> = T extends {[key: string]: infer U} ? U : never;
type ActionObjectTypes = ReturnType<InferValueTypes<typeof actions>>;

type car = {
        id: number,
        name: string,
        type: string
    }

const cars: car[] = []
const selectedCars: number[] = []

const initialState = {
    cars,
    newCar: {
        name: '',
        type: 'CAR'
    },
    selectedCars,
    carIsSelect: false,
    selectedAll: false,
    editCar:{
        id: -1,
        name: '',
        type: ''
    }

}

const carsReducer =  (state = initialState, action: ActionObjectTypes) => { 
    switch (action.type){
        case CarsActions.ADD_CAR:
            return Object.assign({}, state);
        case CarsActions.UPDATE_CAR:
            return state;
        case CarsActions.DELETE_CARS:
            state.selectedCars = []
            state.selectedAll = false
            return Object.assign({}, state);
        case CarsActions.FETCH_CARS:
            return state;
        case CarsActions.REQUEST_CARS_SUCSESS:
            state.cars = action.data
            return Object.assign({}, state);
        case CarsActions.SELECT_CAR:
            state.selectedCars = state.selectedCars.concat(action.id);
            state.carIsSelect = false;
            state.selectedAll = false;
            return Object.assign({}, state);
        case CarsActions.UNSELECT_CAR:
            const tmpId = state.selectedCars.indexOf(action.id);
            state.selectedCars = state.selectedCars.slice(0, tmpId).concat(state.selectedCars.slice(tmpId+1));
            state.carIsSelect = false;
            state.selectedAll = false;
            return Object.assign({}, state);
        case CarsActions.SELECT_ALL_CARS:
            state.selectedAll = !state.selectedAll;
            if (!state.selectedAll){state.selectedCars = []}
            else{state.selectedCars = state.cars.map(item=>item.id);}
            return Object.assign({}, state);
        case CarsActions.START_CHANGE_CAR_RECORD:
            state.editCar = action.data;
            return Object.assign({}, state);
        case CarsActions.EDIT_CAR:
            state.editCar = action.data;
            return Object.assign({}, state);
        default:
            return state;
    }
}

export default carsReducer;