import {CarsActions} from '../constants/ActionTypes';

function inferLiteral<U, T extends U>(arg: T) :T {return arg}
function inferLiteralFromString<T extends string>(arg: T) :T {return inferLiteral<string, T> (arg)}

type DataType={
  name: string,
  type: string
}

type Car = {
  id: number,
  name: string,
  type: string
}

export function addCar(data: DataType) {
  return {
    type: inferLiteralFromString( CarsActions.ADD_CAR),
    data
  };
}

export function updateCar(data: Car) {
  return {
    type: inferLiteralFromString( CarsActions.UPDATE_CAR),
    data
  };
}

export function deleteCars(ids: Array<number>) {
  return {
    type: inferLiteralFromString(CarsActions.DELETE_CARS),
    ids
  };
}

export function fetchCars() {
  return {
    type: inferLiteralFromString(CarsActions.FETCH_CARS),
  };
}

export function requestCarsSucsess(data: any){
  return {
    type: inferLiteralFromString(CarsActions.REQUEST_CARS_SUCSESS),
    data
  }
}

export function selectCar(id: number){
  return {
    type: inferLiteralFromString(CarsActions.SELECT_CAR),
    id
  }
}

export function unSelectCar(id: number){
  return {
    type: inferLiteralFromString(CarsActions.UNSELECT_CAR),
    id
  }
}


export function selectAll(){
  return{
    type: inferLiteralFromString(CarsActions.SELECT_ALL_CARS)
  }
}

export function startChangeCarRecord(data: Car) {
  return{
    type: inferLiteralFromString(CarsActions.START_CHANGE_CAR_RECORD),
    data
  }
}

export function changeCar(data: Car){
  return{
    type: inferLiteralFromString(CarsActions.EDIT_CAR),
    data
  }
}