import {createStore, combineReducers, applyMiddleware} from "redux";
import CarsReducer from './reducers/CarsReducer';
import sagas from './sagas'
import createSagaMiddleware from 'redux-saga';
const reducer = combineReducers({
    cars: CarsReducer,
}
);

export type AppStateType = ReturnType<typeof reducer>;
const sagaMiddleware = createSagaMiddleware();
let store = createStore(reducer, applyMiddleware(sagaMiddleware));
sagaMiddleware.run(sagas)
export default store;   