import { call, put, takeEvery } from 'redux-saga/effects'
import * as actions from '../actions/CarsActions'
import {CarsActions} from '../constants/ActionTypes';
import axios from 'axios'
type AddCarTypes = ReturnType<typeof actions.addCar>;
type DeleteCarTypes = ReturnType<typeof actions.deleteCars>
type UpdateCarTypes = ReturnType<typeof actions.updateCar>
export function* watchRequestCars() {
    yield takeEvery (CarsActions.FETCH_CARS, fetchCarsAsync)
}

function* fetchCarsAsync(){
    try{
        const data = yield call( () => axios.get('http://localhost:8888/places').then( (response : any)=> {
            return response;
          })
          .catch(function (error) {
            console.log(error);
          }))
          console.log(data.data)
        yield put(actions.requestCarsSucsess(data.data))
    } catch (err){
        yield console.log(err)
    }
}

export function* watchPutCars() {
    yield takeEvery (CarsActions.ADD_CAR, putCarsAsync)
}

function* putCarsAsync(action: AddCarTypes ){
    try{
        const carData = {
            'carName': action.data.name,
            'carType': action.data.type
        }
        const data = yield call (() => axios.post('http://localhost:8888/places', carData, {headers:{
            'accept': '*/*', 
            'Content-Type': 'application/json'
        }})
        .then((response: any) => {
            return response;
          })
          .catch(function (error) {
            console.log(error);
          }))
        console.log(data.data)
        yield put(actions.fetchCars())
    } catch (err){
        yield console.log(err)
    }
}

export function* watchDeleteCars(){
    yield takeEvery (CarsActions.DELETE_CARS, deleteCars)
}

function* deleteCars(action: DeleteCarTypes){
    for(let id of action.ids){
        const url = 'http://localhost:8888/places/'+id
        const data = yield call (() => axios.delete(url).then( (response : any)=> {
            return response;
          })
          .catch(function (error) {
            console.log(error);
          }))
          console.log(data)
    }
    yield put(actions.fetchCars())
}

export function* watchUpdateCar(){
    yield takeEvery (CarsActions.UPDATE_CAR, updateCar)
}

function* updateCar(action: UpdateCarTypes){
        const url = 'http://localhost:8888/places/'+action.data.id
        const dataDelete = yield call (() => axios.delete(url).then( (response : any)=> {
            return response;
          })
          .catch(function (error) {
            console.log(error);
          }))
          console.log(dataDelete)
        
        const carData = {
            'carName': action.data.name,
            'carType': action.data.type
        }
        const dataAdd = yield call (() => axios.post('http://localhost:8888/places', carData, {headers:{
            'accept': '*/*', 
            'Content-Type': 'application/json'
        }})
        .then((response: any) => {
            return response;
          })
          .catch(function (error) {
            console.log(error);
          }))
          console.log(dataAdd)
          yield put(actions.fetchCars())
}