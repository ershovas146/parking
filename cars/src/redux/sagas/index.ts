import { all, spawn } from 'redux-saga/effects'
import * as CarsSaga from './CarsSaga'


export default function* rootSaga() {
    yield all([
      spawn(CarsSaga.watchRequestCars),
      spawn(CarsSaga.watchPutCars),
      spawn(CarsSaga.watchDeleteCars),
      spawn(CarsSaga.watchUpdateCar),
    ])
  }