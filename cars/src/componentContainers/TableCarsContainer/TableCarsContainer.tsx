import TableCars from '../../components/Tables/TableCars/TableCars';
import { AppStateType } from '../../redux/store';
import { connect, ConnectedProps} from 'react-redux'
import {selectAll,selectCar,unSelectCar, deleteCars, startChangeCarRecord, changeCar, updateCar} from '../../redux/actions/CarsActions'
import {Dispatch} from 'redux'


type Record = {
  id: number,
  name: string,
  type: string
}

const mapStateToProps = (state: AppStateType) => {
  return {
    rows: state.cars.cars,
    selected: state.cars.selectedCars,
    selectedAll: state.cars.selectedAll,
    rowIsSelected: state.cars.carIsSelect,
    editRecord: state.cars.editCar
  }
};
const mapDispatchToProps = (dispatch: Dispatch) => {
  return {
    selectRow: (id: number) => {dispatch(selectCar(id))},
    unSelectRow: (id: number) => {dispatch(unSelectCar(id))},
    changeSelectAll: () => {dispatch(selectAll())},
    deleteRows: (ids: number[]) => {dispatch(deleteCars(ids))},
    startChangeRecord: (record: Record) =>{dispatch(startChangeCarRecord(record))},
    changeCar: (record: Record) => {dispatch(changeCar(record))},
    updateCar: (record: Record) => {dispatch(updateCar(record))}
  }
}

const connector = connect(mapStateToProps, mapDispatchToProps)

export type PropsType = ConnectedProps<typeof connector>

export default connector(TableCars)