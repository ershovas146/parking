import AddRecordForm from '../../components/AddRecordForm/AddRecordForm'
import { connect, ConnectedProps } from 'react-redux';
import * as actions from '../../redux/actions/CarsActions'
import {Dispatch} from 'redux'

type InferValueTypes<T> = T extends {[key: string]: infer U} ? U : never;
type ActionObjectTypes = ReturnType<InferValueTypes<typeof actions>>;
type DataType = {
  name: string,
  type: string
}
const mapDispatchToProps = (dispatch: Dispatch<ActionObjectTypes>) =>{
  return{
    addRecord: (data: DataType) =>{dispatch(actions.addCar(data))},
  }
}

const connector = connect(null, mapDispatchToProps)

export type PropsType = ConnectedProps<typeof connector>

const TableCarsContainer = connector(AddRecordForm);

export default TableCarsContainer