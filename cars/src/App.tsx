import * as React from 'react';
import MainPage from './components/MainPage/MainPage';
import Navigation from './components/Navigation/Navigation';
import Header from './components/Header/Header';
import './App.css'
const App = () => {
  return (
    <div className="App">
      <Navigation />
      <Header />
      <MainPage />
    </div>
  );
}

export default App;
