import * as React from 'react';
import { createStyles, makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(() =>
    createStyles({
        post: {
            overflow: 'auto',
            background: 'white',
            height: '100%',
            boxShadow: '0px 2px 1px rgba(0, 0, 0, 0.2)',
            display: 'flex',
            flexDirection: 'column'
        },
        image: {
            backgroundImage: 'url(/commercial-services2.png)',
            height: 129
        },
        span: {
            fontFamily: 'Roboto',
            fontStyle: 'normal',
            fontWeight: 'normal',
            fontSize: 12,
            lineHeight: '150%',
            marginLeft: 30,
            marginTop: 10,
            marginBottom: 17,
            color: '#757575;'
        },
        header:{
            fontFamily: 'Roboto',
            fontStyle: 'normal',
            fontWeight: 'normal',
            fontSize: 14,
            lineHeight: '16px',
            color: '#42484D',
            marginLeft: 30,
            marginTop: 30,
        },
        details:{
            marginLeft: 30,
            fontFamily: 'Roboto',
            fontStyle: 'normal',
            fontWeight: 'normal',
            fontSize: 14,
            lineHeight: '150%',
            textTransform: 'uppercase',
            color: '#1976D2',
            cursor: 'pointer',
            '&:hover':{
                textDecoration: 'underline'
            }
        }

    }),
);

const Post = ()=>{
    const classes = useStyles()
    return(
        <div className = {classes.post}>
            <div className ={classes.image}></div>
            <span className={classes.header}>Lorem ipsum dolor sit amet</span>
            <span className={classes.span}>Lorem ipsum dolor sit amet,<br/> consectetur adipiscing elit, sed do<br/> eiusmod tempor incididunt ut labore.</span> 
            <a href= '/#' className={classes.details}>ПОДРОБНЕ</a>
        </div>
    )
}

export default Post;