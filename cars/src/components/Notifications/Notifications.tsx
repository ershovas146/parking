import * as React from 'react';
import ControlPointIcon from '@material-ui/icons/ControlPoint';
import ThumbUpOutlinedIcon from '@material-ui/icons/ThumbUpOutlined';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import { createStyles, makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(() =>
    createStyles({
        list:{
            padding: 0
        },
        icon:{
            color: '#1976D2',
        },
        notification_block: {
            overflow: 'auto',
            background: 'white',
            height: '100%',
            boxShadow: '0px 2px 1px rgba(0, 0, 0, 0.2)'
        },
        head:{
            margin: 0,
            marginLeft: 29,
            marginTop: 34,
        }
    }),
);

const Notifications = () =>{
    const classes = useStyles()
    return (
        <div className={classes.notification_block} >
            <h3 className={classes.head}>Уведомления</h3>
            <List className={classes.list}>
                {[1,2,3,4].map((text, index) => (
                    <ListItem key={text} >
                        <ListItemIcon className={classes.icon} >{index === 0 ? <ControlPointIcon/> : <ThumbUpOutlinedIcon />}</ListItemIcon>
                        <ListItemText  primary='Lorem ipsum sit amet' secondary="12:45" />
                    </ListItem>
                ))}
            </List>
        </div>
    )
}

export default Notifications;