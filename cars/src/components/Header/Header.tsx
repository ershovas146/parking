import * as React from 'react';
import { createStyles, makeStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';

const drawerWidth = 240;

const useStyles = makeStyles(() =>
    createStyles({
        appBar: {
            width: `calc(100% - ${drawerWidth}px)`,
            marginLeft: drawerWidth,
        },
        toolbar: {
            background: '#1976D2'
        }
    })
)

const Header = () => {
    const classes = useStyles();
    return (
    <AppBar position="fixed" className={classes.appBar}>
        <Toolbar className = {classes.toolbar}>
            <Typography variant="h6" noWrap >
                Добро Пожаловать
            </Typography>
        </Toolbar>
    </AppBar>
    );

}

export default Header;