import * as React from 'react';
import { createStyles, makeStyles } from '@material-ui/core/styles';
import OutlinedInput from '@material-ui/core/OutlinedInput';
import InputLabel from '@material-ui/core/InputLabel';
import InputAdornment from '@material-ui/core/InputAdornment';
import FormControl from '@material-ui/core/FormControl';
import Button from '@material-ui/core/Button';
import AddCircleOutlineIcon from '@material-ui/icons/AddCircleOutline';
import { PropsType } from '../../componentContainers/AddRecordFormContainer/AddRecordFormContainer';
import MenuItem from '@material-ui/core/MenuItem';
import Select from '@material-ui/core/Select';
const useStyles = makeStyles(() =>
    createStyles({
        add_block: {
            display: 'grid',
            alignItems: 'center',
            background: 'white',
            boxShadow: '0px 2px 1px rgba(0, 0, 0, 0.2)',
            padding: '20px 40px',
            boxSizing: 'border-box',
            gridTemplateColumns: '0.5fr 1.5fr 5fr'
        },
        form: {
            display: 'flex',
            justifyContent: 'space-between'
        },
        lable: {
            fontFamily: 'Roboto',
            fontStyle: 'normal',
            fontWeight: 500,
            fontSize: 16,
            lineHeight: '19px',
            color: '#42484D',
        },
        submit: {
            width: 150,
            background: '#1976D2',
            color: '#FFFFFF',
            '&:hover':{
                background: '#1976D2',
            }
        },
        input: {
            width: 250
        },
        plus:{
            fill: '#1976D2',
            height: 60,
            width: 60
        }

    }),
);


const AddRecordForm = (props: PropsType)=>{
    const [name, setName] = React.useState('');
    const [type, setType] = React.useState('CAR');
    const newRecord = () => {
        const data = {
            name: name,
            type: type
        }
        props.addRecord(data)
    }
    const nameChange = (event: React.ChangeEvent<{ value: unknown }>) => {
        setName(event.target.value as string);
      };
    const typeChange = (event: React.ChangeEvent<{ value: unknown }>) => {
        setType(event.target.value as string);
      };
    const classes = useStyles()
    return(
        <div className={classes.add_block}>
            <AddCircleOutlineIcon fontSize='large' className={classes.plus} />
            <span className={classes.lable}>Добавить автомобиль</span>
            <form  action="/#" className={classes.form} autoComplete="off" onSubmit={newRecord}>
            <FormControl fullWidth variant="outlined" size='small' className={classes.input}>
            <InputLabel htmlFor="name">Название</InputLabel>
            <OutlinedInput
                id="name"
                startAdornment={<InputAdornment position="start"></InputAdornment>}
                labelWidth={60}
                value = {name}
                onChange = {nameChange}
            />
            </FormControl>
            <FormControl fullWidth variant="outlined" size='small' className={classes.input}>
            <InputLabel id="demo-simple-select-outlined-label">Type</InputLabel>
            <Select
            labelId="demo-simple-select-outlined-label"
            id="demo-simple-select-outlined"
            value={type}
            onChange = {typeChange}
            label="type"
            >
            <MenuItem value={'CAR'}>Легковая</MenuItem>
            <MenuItem value={'CARGO'}>Грузовая</MenuItem>
            </Select>
            </FormControl>
            <Button variant="contained" type='submit' className={classes.submit}>
                Добавить
            </Button>
            </form>
        </div>
    )
}

export default AddRecordForm;