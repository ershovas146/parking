import * as React from 'react';
import { ThemeProvider, createMuiTheme } from '@material-ui/core/styles';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import EmailOutlinedIcon from '@material-ui/icons/EmailOutlined';
import DesktopWindowsIcon from '@material-ui/icons/DesktopWindows';
import PlayCircleOutlineIcon from '@material-ui/icons/PlayCircleOutline';
import SettingsOutlinedIcon from '@material-ui/icons/SettingsOutlined';
import DescriptionOutlinedIcon from '@material-ui/icons/DescriptionOutlined';


const theme = createMuiTheme({
    typography: {
        fontFamily: 'Open Sans',
        fontSize: 14,
        fontWeightLight: 600,
        fontWeightRegular: 600,
        fontWeightMedium: 600,
    }
  });


interface Prop {
    items: string[];
  }
  
const getIcon = (text : string) =>{
    switch (text) {
        case 'Choose':
            return <DesktopWindowsIcon />;
        case 'Your':
            return <DescriptionOutlinedIcon />;
        case 'Destiny':
            return <PlayCircleOutlineIcon />;
        case 'Fight':
            return <EmailOutlinedIcon />;
        case 'Settings':
            return <SettingsOutlinedIcon />;
        default:
            return <div />;
    }
}

const NavigationList = (props: Prop) => {
    return (
        <ThemeProvider theme = {theme}>
        <List >
            {props.items.map((text, index) => (
                <ListItem button key={text} >
                    <ListItemIcon>{ getIcon(text)}</ListItemIcon>
                    <ListItemText  primary={text}/>
                </ListItem>
            ))}
        </List>
        </ThemeProvider>
    );
}

export default NavigationList