import * as React from 'react';
import { createStyles, makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(() =>
  createStyles({
    TT: {
        width: 30,
        height: 33,
        left: 2,
    },
    TTr: {
        marginTop: -10,
        width: 28.67,
        height: 19.25,
        left: 25.33,
        marginBottom: 14,
        marginLeft: -3
    }
  }),
);

  

const Logo = () => {
    const classes = useStyles();
    return(
        <div>
            <img alt="Not Found" src = '/TT.png' className = {classes.TT} />
            <img alt="Logo" src= '/TTr.png' className = {classes.TTr } />
        </div>
    );
}

export default Logo;