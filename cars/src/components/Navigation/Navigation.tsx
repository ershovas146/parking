import * as React from 'react';
import { createStyles, makeStyles } from '@material-ui/core/styles';
import Drawer from '@material-ui/core/Drawer';
import Divider from '@material-ui/core/Divider';
import NavigationList from './NavigationList/NavigationList'
import Logo from './Logo/Logo';

const drawerWidth = 240;

const useStyles = makeStyles(() =>
    createStyles({
        drawer: {
        width: drawerWidth,
        flexShrink: 0,
        },
        drawerPaper: {
        width: drawerWidth,
        },
        toolbar: {
            height: 72,
            display: 'flex',
            justifyContent: 'center',
            alignItems: 'flex-end'
        }
    }),
);

const Navigation = () => {
    const classes = useStyles();
    return (
      <div>
        <Drawer
            className={classes.drawer}
            variant="permanent"
            classes={{
            paper: classes.drawerPaper,
            }}
            anchor="left"
        >
            <div className={classes.toolbar}><Logo /></div>
            <NavigationList items={['Choose','Your','Destiny','Fight']}/>
            <Divider />
            <NavigationList items={['Settings']}/>
        </Drawer>
      </div>
    );
}

export default Navigation