import * as React from 'react';
import { createStyles, makeStyles } from '@material-ui/core/styles';
import Notifications from '../Notifications/Notifications';
import  Tables  from '../Tables/Tables';
import Post from '../Post/Post';
import Advertesting from '../Advertesting/Advertesting'
import AddRecordFormContainer from '../../componentContainers/AddRecordFormContainer/AddRecordFormContainer';

const NavigationWidth = 240;
const HeaderHeight = 60;
const useStyles = makeStyles(() =>
    createStyles({
      body: {
        marginTop: HeaderHeight, 
        marginLeft: NavigationWidth,
        width: `calc(100% - ${NavigationWidth}px)`,
        height: `calc(100%)`,
        background: '#E5E5E5',
        gridTemplateRows: '281px 291px 100px ',
        display: 'grid',
        gridGap: 22,
        paddingLeft: 60,
        paddingRight: 60,
        paddingTop: 40,
        boxSizing: 'border-box'
      },
      '@media (max-height: 750px)':  {
        body:{
          height: 'auto'
        } 
      },
      first:{
        gridTemplateColumns: '709px 344px',
        display: 'grid',
        gridGap: 22,
      },
      second:{
        gridTemplateColumns: '250px 250px 530px',
        display: 'grid',
        gridGap: 22,
      },
      third:{
        gridTemplateColumns: '1075px',
        display: 'grid',
      }
    }),
);


const MainPage = () => {
  const classes = useStyles();
    return (
      <main className={classes.body}>
        <section className={classes.first}>
          <Tables />
          <Notifications />
        </section>
        <section className={classes.second}>
          <Post /> <Post /> <Advertesting />
        </section>
        <section className={classes.third}>
          <AddRecordFormContainer/>
        </section>
      </main>
    );
}

export default MainPage