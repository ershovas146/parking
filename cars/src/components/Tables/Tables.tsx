import * as React from 'react';
import AppBar from '@material-ui/core/AppBar';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';
import {  makeStyles, withStyles } from '@material-ui/core/styles';
import TableCarsContainer from '../../componentContainers/TableCarsContainer/TableCarsContainer';

interface TabPanelProps {
    children?: React.ReactNode;
    index: any;
    value: any;
  }
 const  a11yProps = (index: any) => {
    return {
      id: `simple-tab-${index}`,
      'aria-controls': `simple-tabpanel-${index}`,
    };
  }

const TabPanel = (props: TabPanelProps) => {
    const { children, value, index, ...other } = props;
  
    return (
      <Typography
        component="div"
        role="tabpanel"
        hidden={value !== index}
        id={`simple-tabpanel-${index}`}
        aria-labelledby={`simple-tab-${index}`}
        {...other}
      >
        {value === index && <Box >{children}</Box>}
      </Typography>
    );
}
interface StyledTabProps {
    label: string;
  }
const AntTab = withStyles(theme => ({
    root: {
      textTransform: 'none',
      minWidth: 72,
      fontWeight: 500,
      marginRight: theme.spacing(4),
      fontFamily: 'Roboto',
      fontSize: '16px',
      lineHeight: '19px',
      opacity: 1
    },
    selected: {},
  }))((props: StyledTabProps) => <Tab disableRipple {...props} />);

const useStyles = makeStyles({
    content:{
        padding: 0
    },
    head: {
        boxShadow: 'none',
        background: '#1976D2'
    }
  });
  
const Tables = () =>{
    const classes = useStyles()
    const [value, setValue] = React.useState(0);
    const handleChange = (event: React.ChangeEvent<{}>, newValue: number) => {
        setValue(newValue);
      };
    return(
        <div>
            <AppBar position="static" className={classes.head}>
                <Tabs value={value} onChange={handleChange} aria-label="simple tabs example">
                <AntTab label="Список автомобилей" {...a11yProps(0)} />
                <AntTab label="Вкладка 2" {...a11yProps(1)} />
                <AntTab label="Вкладка 3" {...a11yProps(2)} />
                </Tabs>
            </AppBar>
            <TabPanel value={value} index={0}>
                <TableCarsContainer/>
            </TabPanel>
            <TabPanel value={value} index={1}>
                Пока работает только вкладка 1
            </TabPanel>
            <TabPanel value={value} index={2}>
                Пока работает только вкладка 1
            </TabPanel>
        </div>
    )
}

export default Tables;