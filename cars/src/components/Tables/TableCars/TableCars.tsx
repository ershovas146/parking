import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import OutlinedInput from '@material-ui/core/OutlinedInput';
import InputAdornment from '@material-ui/core/InputAdornment';
import Table from '@material-ui/core/Table';
import IconButton from '@material-ui/core/IconButton';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import Checkbox from '@material-ui/core/Checkbox';
import DeleteIcon from '@material-ui/icons/Delete';
import MenuItem from '@material-ui/core/MenuItem';
import Select from '@material-ui/core/Select';
import CheckBoxIcon from '@material-ui/icons/CheckBox';
import {PropsType} from '../../../componentContainers/TableCarsContainer/TableCarsContainer'
const useStyles = makeStyles({
  table: {
    minWidth: 650,
  },
  container: {
    maxHeight: 234
  },
  trash_th:{
    display: 'flex',
    justifyContent: 'space-between'
  },
  trash: {
    padding: 0,
  },
  editName: {
    padding: 0,
    maxWidth: 100,
    position: 'inherit',
    fontSize: '0.875rem',
    '& input':{
      outline: 0,
      border: 0,
      padding: 0,
      marginLeft: -10,
      fontFamily: '"Roboto", "Helvetica", "Arial", sans-serif',
      fontWeight: 400,
    }
  },
  editType: {
    padding: 0,
    maxWidth: 200,
    position: 'inherit',
    fontSize: '0.875rem',
    '& div':{
      psdding: 0  
    },
    '& input':{
      outline: 0,
      border: 0,
      padding: 0,
      marginLeft: -10,
      fontFamily: '"Roboto", "Helvetica", "Arial", sans-serif',
      fontWeight: 400,
    }
  },
  change_box:{
    display: 'flex',
    justifyContent: 'space-between'
  },
  changeOK:{
    padding: 0
  }
});

const TableCars= (props: PropsType)=> {
  const classes = useStyles();
  const changeSelect = (checked: boolean, id: number)=>{
    if(checked){
      props.selectRow(id)
    } else{
      props.unSelectRow(id)
    }
  }

  const nameChange = (id:number, event: React.ChangeEvent<{ value: unknown }>) => {
    changeCar(id, event.target.value as string, props.editRecord.type)
  }
  const typeChange = (id: number, event: React.ChangeEvent<{ value: unknown }>) => {
    changeCar(id, props.editRecord.name, event.target.value as string,)
  }

  const changeCar  = (id: number, name:string, type: string) => {
    const record = {
      id: id,
      name: name,
      type: type
    }
    props.changeCar(record);
  }
  return (
    <TableContainer component={Paper} className={classes.container}>
      <Table className={classes.table} stickyHeader aria-label="sticky table">
        <TableHead>
          <TableRow>
            <TableCell padding="checkbox"  align='right' style={{ minWidth: 60 }}>
            <Checkbox
              checked = {props.selectedAll}
              onChange={props.changeSelectAll}
            />
            </TableCell>
            <TableCell>Название</TableCell>
            <TableCell className={classes.trash_th}>Тип
            { props.selected.length > 0  &&
              <IconButton onClick={()=>{props.deleteRows(props.selected)}} className={classes.trash} aria-label="upload picture" component="span">
              <DeleteIcon />
              </IconButton>
            }
            </TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          { props.rows.map(row => (
            <TableRow onDoubleClick = {()=>{props.startChangeRecord(row)}} key={row.id}>
                <TableCell padding="checkbox" align='right'>
                    <Checkbox
                    checked = {props.selected.indexOf(row.id) !== -1}
                    onChange = {(event:React.ChangeEvent<HTMLInputElement>)=>{changeSelect(event.target.checked, row.id)}}
                    onDoubleClick = {(event:React.MouseEvent)=>{event.stopPropagation()}}
                    />
                </TableCell>
              <TableCell component="th" scope="row">
                { props.editRecord.id === row.id &&
                <OutlinedInput
                    className={classes.editName}
                    id="name"
                    startAdornment={<InputAdornment position="start"></InputAdornment>}
                    value = {props.editRecord.name}
                    onChange = {(event: React.ChangeEvent<{ value: unknown }>)=>{nameChange(row.id, event)}}
                />
                }
                { props.editRecord.id !== row.id &&
                row.name
                }
              </TableCell>
              <TableCell >
                { props.editRecord.id === row.id &&
                <div className={classes.change_box}>
                <Select
                className={classes.editType}
                labelId="demo-simple-select-outlined-label"
                id="demo-simple-select-outlined"
                value={props.editRecord.type}
                onChange = {(event: React.ChangeEvent<{ value: unknown }>)=>{typeChange(row.id, event)}}
                >
                  <MenuItem value={'CAR'}>легковая</MenuItem>
                  <MenuItem value={'CARGO'}>грузовая</MenuItem>
                </Select>
                <IconButton onClick={()=>{props.updateCar(props.editRecord)}} className={classes.changeOK} component="span">
                <CheckBoxIcon />
                </IconButton>
                </div>
                }
                {props.editRecord.id !== row.id &&
                <span>
                {row.type === 'CAR'? 'легковая': 'грузовая'}
                </span>
                }
              </TableCell>
            </TableRow>
          ))}
        </TableBody>
      </Table>
    </TableContainer>
  );
}
export default TableCars