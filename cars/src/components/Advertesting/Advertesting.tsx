import * as React from 'react';
import { createStyles, makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(() =>
    createStyles({
        main: {
            height: '100%',
            backgroundImage: 'url(/car.png)',
            backgroundColor: '#1976D2',
            backgroundPosition: '209px 57px',
            backgroundRepeat: 'no-repeat',
            display: 'flex',
            flexDirection: 'column',
            boxShadow: '0px 2px 1px rgba(0, 0, 0, 0.2)'
        },
        span: {
            fontFamily: 'Roboto',
            fontStyle: 'normal',
            fontWeight: 'normal',
            fontSize: 12,
            lineHeight: '18px',
            color: '#FFFFFF',
            marginLeft: 49,
            marginTop: 45,       
        },
        header:{
            fontFamily: 'Roboto',
            fontStyle: 'normal',
            fontWeight: 'bold',
            fontSize: 25,
            lineHeight: '29px',
            color: '#FFFFFF',
            marginLeft: 49,
            marginTop: 70,  
        },
    }),
);

const Post = ()=>{
    const classes = useStyles()
    return(
        <div className={classes.main}>
            <span className={classes.header}>2141 со<br/> скидкой 50%</span>
            <span className={classes.span}>Lorem ipsum dolor sit amet,<br/> consectetur adipiscing elit, sed do<br/> eiusmod tempor incididunt ut labore.</span> 
        </div>
    )
}

export default Post;